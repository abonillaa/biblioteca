package com.biblioteca.bl;

public class Administrativo extends Usuario{

    private char tipoNombramiento;
    private int horasAsignadas;

    public Administrativo(int ID, String nombre, String apellido, String tipoUsuario, char tipoNombramiento, int horasAsignadas) {
        super(ID, nombre, apellido, tipoUsuario);
        this.tipoNombramiento = tipoNombramiento;
        this.horasAsignadas = horasAsignadas;
    }

    public char getTipoNombramiento() {
        return tipoNombramiento;
    }

    public void setTipoNombramiento(char tipoNombramiento) {
        this.tipoNombramiento = tipoNombramiento;
    }

    public int getHorasAsignadas() {
        return horasAsignadas;
    }

    public void setHorasAsignadas(int horasAsignadas) {
        this.horasAsignadas = horasAsignadas;
    }
}
