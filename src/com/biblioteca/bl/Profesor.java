package com.biblioteca.bl;

import java.time.LocalDate;

public class Profesor extends Usuario{

    private String tipoContrato;
    private LocalDate fechaContratacion;

    public Profesor(int ID, String nombre, String apellido, String tipoUsuario, String tipoContrato, LocalDate fechaContratacion) {
        super(ID, nombre, apellido, tipoUsuario);
        this.tipoContrato = tipoContrato;
        this.fechaContratacion = fechaContratacion;
    }

    public String getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public LocalDate getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(LocalDate fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }
}
