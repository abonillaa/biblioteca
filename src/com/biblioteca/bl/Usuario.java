package com.biblioteca.bl;

public class Usuario {

    private int ID;
    private String nombre;
    private String apellido;
    private String tipoUsuario;

    public Usuario(int ID, String nombre, String apellido, String tipoUsuario) {
        this.ID = ID;
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipoUsuario = tipoUsuario;
    }



    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
}
