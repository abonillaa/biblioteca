package com.biblioteca.bl;

public class Otro extends Material {

    private String descripcion;

    public Otro(int ID, int anno, int mes, int dia, boolean restringido, String tema, String tipoMaterial, String descripcion) {
        super(ID, anno, mes, dia, restringido, tema, tipoMaterial);
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
