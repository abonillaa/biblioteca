package com.biblioteca.bl;

public class Estudiante {

    private String carrera;
    private int numeroCreditos;

    public Estudiante(String carrera, int numeroCreditos) {
        this.carrera = carrera;
        this.numeroCreditos = numeroCreditos;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public int getNumeroCreditos() {
        return numeroCreditos;
    }

    public void setNumeroCreditos(int numeroCreditos) {
        this.numeroCreditos = numeroCreditos;
    }
}
