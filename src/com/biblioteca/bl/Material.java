package com.biblioteca.bl;

import java.time.LocalDate;

public class Material {

    private int ID;
    private LocalDate fechaCompra;
    private boolean restringido;
    private String tema;
    private String tipoMaterial;

    public Material(int ID, int anno, int mes, int dia, boolean restringido, String tema, String tipoMaterial) {
        this.ID = ID;
        this.fechaCompra = LocalDate.of(anno,mes,dia);
        this.restringido = restringido;
        this.tema = tema;
        this.tipoMaterial = tipoMaterial;
    }

    public int getID() {
        return ID;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public boolean isRestringido() {
        return restringido;
    }

    public String getTema() {
        return tema;
    }
}
