package com.biblioteca.bl;

public class Audio extends Material {

    private String formato;
    private String duracion;
    private String idioma;

    public Audio(int ID, int anno, int mes, int dia, boolean restringido, String tema, String tipoMaterial, String formato, String duracion, String idioma) {
        super(ID, anno, mes, dia, restringido, tema, tipoMaterial);
        this.formato = formato;
        this.duracion = duracion;
        this.idioma = idioma;
    }

    public String getFormato() {
        return formato;
    }

    public String getDuracion() {
        return duracion;
    }

    public String getIdioma() {
        return idioma;
    }
}
