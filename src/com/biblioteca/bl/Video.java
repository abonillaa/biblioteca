package com.biblioteca.bl;

public class Video extends Audio {

    private String director;

    public Video(int ID, int anno, int mes, int dia, boolean restringido, String tema, String tipoMaterial, String formato, String duracion, String idioma, String director) {
        super(ID, anno, mes, dia, restringido, tema, tipoMaterial, formato, duracion, idioma);
        this.director = director;
    }

    public String getDirector() {
        return director;
    }
}
